import React, { useState } from 'react';
import Header from '../shared/Header.js'
import TranslationInput from '../functionality/TranslationInput.js'
import TranslationOutput from '../functionality/TranslationOutput.js'
import { getStorage, setStorage, deleteStorage} from '../utils/storage.js';
import './TranslationPage.css';
import { useHistory } from "react-router-dom";
import { Redirect } from 'react-router-dom';

function TranslationPage(){
    let history = useHistory();

    const userName = getStorage('name');
    const [currentTranslation, setCurrentTranslation] = useState();

    function saveInput(input){
        //Input validation, length
        if (input.length > 20){
            alert('Input longer than 20 characters, please shorten!');
            return;
        }
        let newInput = '';

        //input validation, is it a letter
        for (let char of input){
            char = char.toLowerCase();
            if (char.charCodeAt(0) > 96  && char.charCodeAt(0) < 123){
                newInput += char;
            } else if (char === ' '){
                newInput += char;
            }
        }

        setCurrentTranslation(newInput);
        //Updates the local storage of the list, if the list is letters
        if (newInput.length > 0){
            const storedList = getStorage('list');
            if (storedList){
                if (storedList.length === 10){
                    let newList = storedList;
                    newList.shift();
                    newList.push(newInput);
                    deleteStorage('list');
                    setStorage('list', newList);
                } else {
                    let newList = storedList;
                    newList.push(newInput);
                    deleteStorage('list');
                    setStorage('list', newList);
                }
            } else {
                let newList = [newInput];
                setStorage('list', newList);
            }
        }
    };

    function goToProfile(){
        history.push('/profile');
    };

    return (
        <div>
            { !getStorage('name') && <Redirect to="/"/>}
            <span id = "username">
                <img id = "usernameLogo" src= "logo.png" alt="user"/>
                <h3 id = "clickable" onClick ={goToProfile}>{userName}</h3>
            </span>
            <div className = "thirdTop top">
                <Header></Header>
                <TranslationInput onTranslationClicked={saveInput}/>
            </div>
            <div id = "paddingTop">
                <div className = "thirdBottom bottom translationBox">
                    <TranslationOutput id = "translationBox" textToBeTranslated={currentTranslation}/>
                </div>
            </div>
        </div>
    );
}

export default TranslationPage;