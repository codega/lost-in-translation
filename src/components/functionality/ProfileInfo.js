import React from 'react';
import {getStorage} from '../utils/storage.js';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup'
import './ProfileInfo.css';


function ProfileInfo(){
    const translations = getStorage('list');
    let translationItems;
    let i = 0;

    if (translations){
        i = i + 1;
        translationItems = translations.map(translation => {
            return <ListGroup.Item key={i} className ="py-0" id = "translationText">{'#' + i++ +': ' + translation}</ListGroup.Item>
        })
    };

    return (
        <Card className = "shadow-lg  bg-white" id = "translationsDisplay" >
        <Card.Body>
            <ListGroup variant="flush">
                {translationItems}
            </ListGroup>
        </Card.Body>
        <Card.Footer id = "footer"/>
        </Card>

    )
};

export default ProfileInfo;