import React, { useState } from 'react';
import { setStorage } from '../utils/storage.js';
import { useHistory } from "react-router-dom";
import './NameBox.css';
import Card from 'react-bootstrap/Card';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';


function NameBox(){
    let history = useHistory();
    const [name, setName] = useState('');

    function nameInputHandler(event){
        setName(event.target.value)
    };

    function nameButtonHandler(event){
        if (name === ''){
            alert('Please enter name');
        } else {
            setStorage('name', name);
            history.push('/translation');
        }
    };

    return (
        <div>
        <Card className = "shadow-lg  bg-white" id = "nameBox">
            <Card.Body id = "centering">
                <InputGroup className="mb-3">
                    <FormControl size= "lg" id = "nameInput" placeholder="What's your name?" onChange = {nameInputHandler}/>
                    <InputGroup.Append>
                    <Button variant="secondary" id = "button-color" onClick = {nameButtonHandler}>→</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Card.Body>
            <Card.Footer id = "purple-footer"></Card.Footer>
        </Card>
        </div>
    );
}

export default NameBox;