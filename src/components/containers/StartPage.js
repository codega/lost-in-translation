import React from 'react';

import Header from '../shared/Header.js'
import NameBox from '../functionality/NameBox.js'
import { Redirect } from 'react-router-dom';
import { getStorage} from '../utils/storage.js';
import './StartPage.css';

function StartPage(){
    return (
        <div>
            { getStorage('name') && <Redirect to="/profile"/>}
            <div id = "overlay">
                <NameBox id = "boxElement"></NameBox>
            </div>        
            <div className ="split top">
                <Header></Header>
                <img id = "logo" src = "logo-hello.png" alt ="Hello logo"></img>
            </div>
            <div className = "split bottom">
            </div>
        </div>
    );
}

export default StartPage;