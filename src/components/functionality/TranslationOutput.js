import React from 'react';
import './TranslationOutput.css'
import Card from 'react-bootstrap/Card';


function TranslationOutput(props){

    let translationImages;

    if (props.textToBeTranslated){
        let textSplice = props.textToBeTranslated;
        const text = textSplice.split('');
        let key = -1;
        
        //Turns string input into JSX-image elements
        translationImages = text.map(char => {
            char = char.toLowerCase();
            if (char === ' '){
                key ++;
                return (<span key={key}>&nbsp;&nbsp;&nbsp;</span>);          
            }
            const imageLink = process.env.PUBLIC_URL + '/resources/' + char + '.png';
            key ++;
            return <img id = "image" key={key} src={imageLink} alt={char}/>;   
        });
    }

    return (
        <Card className = "shadow-lg  bg-white" id = "translationCard" >
            <Card.Body>
                {translationImages}
            </Card.Body>
            <Card.Footer id = "footer"/>
        </Card>
    )
};

export default TranslationOutput;

