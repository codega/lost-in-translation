import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import ProfilePage from './components/containers/ProfilePage.js'
import StartPage from './components/containers/StartPage.js'
import TranslationPage from './components/containers/TranslationPage.js'

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/">
            <StartPage />
          </Route>
          <Route path="/translation">
            <TranslationPage />
          </Route>
          <Route path="/profile">
            <ProfilePage />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
