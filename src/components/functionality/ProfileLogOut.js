import React from 'react';
import Button from 'react-bootstrap/Button';


import {deleteStorage} from '../utils/storage.js';
import { useHistory } from "react-router-dom";

function ProfileLogOut(){
    let history = useHistory();

    function logOutButtonClicked(){
        let logout = window.confirm('Are you sure you want to log out?');
        if (logout){
            deleteStorage('name');
            deleteStorage('list');
            history.push('/');    
        }
    };

    return (
        <div>
            <Button variant="secondary" id ="button-color" onClick={logOutButtonClicked}>LOG OUT</Button>
        </div>
    )
}
export default ProfileLogOut;