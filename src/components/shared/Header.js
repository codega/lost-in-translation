import React from 'react';
import './Header.css'
import { useHistory } from "react-router-dom";


function Header(){
    const history = useHistory();
    
    function goToTranslation(){
        history.push('/translation')   
    }

    return (
        <div id ="headerBackground">
            <h1 id="headerText" onClick ={goToTranslation}>Lost in Translation</h1>
        </div>
    );
}

export default Header;