import React from 'react';

import './ProfilePage.css';

import Header from '../shared/Header.js'

import ProfileInfo from '../functionality/ProfileInfo.js'
import ProfileLogOut from '../functionality/ProfileLogOut.js'
import { Redirect } from 'react-router-dom';
import { getStorage} from '../utils/storage.js';

function ProfilePage(){
    const userName = getStorage('name');

    return (
        <div>
            {  !getStorage('name') && <Redirect to="/"/>}
            <span id = "username">
                <img id = "usernameLogo" src= "logo.png" alt="user"/>
                <h3 id = "headerUsername" >{userName}</h3>
            </span>
            <div className = "thirdTop top">
                <Header></Header>
                <div id = "logOutButtonDiv">
                    <ProfileLogOut></ProfileLogOut>
                </div>
            </div>
            <div className = "thirdBottom bottom">
                <ProfileInfo></ProfileInfo>
            </div>
        </div>
    );
}

export default ProfilePage;