import React, { useState } from 'react';
import './TranslationInput.css';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';


function TranslationInput(props){

    const [translation, setTranslation] = useState('');
    
    function translationChange(event){
        setTranslation(event.target.value);
    };

    function translationButtonClick(){
        props.onTranslationClicked(translation);
    };
 
    return (
        <div id ="translationInputBox">
            <InputGroup className="mb-3">
            <FormControl size= "lg" id = "translationInput" onChange={translationChange} placeholder="Type text to be translated here:"/>
                <InputGroup.Append>
                <Button variant="secondary" id = "translateButton" onClick={translationButtonClick}>→</Button>
                </InputGroup.Append>
            </InputGroup>
        </div>
    )
};

export default TranslationInput;